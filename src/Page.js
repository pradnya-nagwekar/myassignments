import Data from "./RequestData";
import Event from './Event';
export default class Page{
  constructor(){
    this.data = []
    this.dataAvailable = new Event();
  }
  fetchData(){
    let data = new Data();
    data.getData('https://jsonplaceholder.typicode.com/photos').then(
      (response)=>{
        this.data = JSON.parse(response);
        console.log("Data available");
        this.dataAvailable.notify();
      },
      (error)=>{
        console.error("Failed",error);
      }
    )
  }

}
