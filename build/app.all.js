/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Event = function () {
    function Event(sender) {
        _classCallCheck(this, Event);

        this._sender = sender;
        this._listeners = [];
    }
    /**
    attach listerner
    */


    _createClass(Event, [{
        key: "attach",
        value: function attach(listener) {
            this._listeners.push(listener);
        }
        /**
        notify all listners listening to Event
        */

    }, {
        key: "notify",
        value: function notify(args) {
            var _this = this;

            this._listeners.forEach(function (item) {
                item(_this._sender, args);
            });
        }
    }]);

    return Event;
}();

exports.default = Event;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Page = __webpack_require__(2);

var _Page2 = _interopRequireDefault(_Page);

var _PhotoGallery = __webpack_require__(4);

var _PhotoGallery2 = _interopRequireDefault(_PhotoGallery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var photoGallery = new _PhotoGallery2.default();
var page = new _Page2.default();
page.dataAvailable.attach(function () {
  photoGallery.model = page.data;
  photoGallery.modelChanged.notify();
  photoGallery.addEventListeners();
});
page.fetchData();
window.onload = function () {
  //filter listeners are added here.
  document.getElementById("filter-id").addEventListener('change', function (e) {
    if (e.target.value == "Odd") {
      photoGallery.model = page.data.filter(function (item) {
        return item.id % 2 != 0;
      });
      photoGallery.modelChanged.notify();
    } else if (e.target.value == "Even") {
      photoGallery.model = page.data.filter(function (item) {
        return item.id % 2 == 0;
      });
      photoGallery.modelChanged.notify();
    } else {
      photoGallery.model = page.data;
      photoGallery.modelChanged.notify();
    }
  });

  document.getElementById('search-btn').addEventListener('click', function (e) {
    var searchString = document.getElementById('filter-title').value;
    photoGallery.model = page.data.filter(function (item) {
      return item.title.includes(searchString);
    });
    photoGallery.modelChanged.notify();
  });
};

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _RequestData = __webpack_require__(3);

var _RequestData2 = _interopRequireDefault(_RequestData);

var _Event = __webpack_require__(0);

var _Event2 = _interopRequireDefault(_Event);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Page = function () {
  function Page() {
    _classCallCheck(this, Page);

    this.data = [];
    this.dataAvailable = new _Event2.default();
  }

  _createClass(Page, [{
    key: 'fetchData',
    value: function fetchData() {
      var _this = this;

      var data = new _RequestData2.default();
      data.getData('https://jsonplaceholder.typicode.com/photos').then(function (response) {
        _this.data = JSON.parse(response);
        console.log("Data available");
        _this.dataAvailable.notify();
      }, function (error) {
        console.error("Failed", error);
      });
    }
  }]);

  return Page;
}();

exports.default = Page;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

//file for http request call
var RequestData = function () {
  function RequestData() {
    _classCallCheck(this, RequestData);
  }

  _createClass(RequestData, [{
    key: 'getData',
    value: function getData(url) {
      return new Promise(function (resolve, reject) {
        var xhttp = new XMLHttpRequest();
        xhttp.open('GET', url);
        xhttp.onload = function () {
          if (xhttp.status == 200) {
            resolve(xhttp.response);
          } else {
            reject(new Error(request.statusText));
          }
        };
        xhttp.onerror = function () {
          reject(new Error('Network Error'));
        };
        //Make a RequestData
        xhttp.send();
      });
    }
  }]);

  return RequestData;
}();

exports.default = RequestData;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Event = __webpack_require__(0);

var _Event2 = _interopRequireDefault(_Event);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var PhotoGallery = function () {
  function PhotoGallery() {
    var _this = this;

    _classCallCheck(this, PhotoGallery);

    //constructor function. Initializes all data
    this.model = [];
    this.pageIndexChanged = new _Event2.default();
    this.paginatorIndexChanged = new _Event2.default();
    this.modelChanged = new _Event2.default();
    this.currentPage = 0;
    this.totalPages = 0;
    this.currentPaginatorIndex = 0;
    this.pageIndexChanged.attach(function () {
      //listener for pageIndexChangedEvent
      _this.renderImageData(_this.currentPage);
    });
    this.paginatorIndexChanged.attach(function () {
      //listerner for paginator index changed event
      _this.renderPaginator(_this.currentPaginatorIndex);
    });

    this.modelChanged.attach(function () {
      //listener for model changed event
      _this.resetModelData();
    });
  }

  _createClass(PhotoGallery, [{
    key: 'resetModelData',
    value: function resetModelData() {
      this.currentPage = 0;
      this.currentPaginatorIndex = 0;
      this.totalPages = parseInt(this.model.length / 15);
      this.pageIndexChanged.notify();
      this.paginatorIndexChanged.notify();
    }

    //This function does the rendering of images when page index changes

  }, {
    key: 'renderImageData',
    value: function renderImageData(from) {
      var _this2 = this;

      var thumbContainers = document.getElementById("photo-gallery-container").children;
      [].forEach.call(thumbContainers, function (item, index) {
        item.children[0].src = _this2.model[from + index].url;
        item.children[1].innerHTML = _this2.model[from + index].title;
        item.children[2].innerHTML = _this2.model[from + index].id;
      });
    }

    //this function render's paginator data

  }, {
    key: 'renderPaginator',
    value: function renderPaginator(from) {
      var paginatorContainer = document.getElementById('paginator').children;
      [].forEach.call(paginatorContainer, function (item, index) {
        item.innerHTML = from + index + 1;
      });
    }

    //add listeners for next, previous, older and newer buttons.

  }, {
    key: 'addEventListeners',
    value: function addEventListeners() {
      var _this3 = this;

      document.getElementById('next').addEventListener('click', function (e) {
        if (_this3.currentPage != _this3.totalPages) {
          _this3.currentPage += 15;
          _this3.currentPaginatorIndex += 1;
          _this3.pageIndexChanged.notify();
          _this3.paginatorIndexChanged.notify();
        }
      });
      document.getElementById('prev').addEventListener('click', function (e) {
        if (_this3.currentPage != 0 && _this3.currentPaginatorIndex > 0) {
          _this3.currentPage -= 15;
          _this3.currentPaginatorIndex -= 1;
          _this3.pageIndexChanged.notify();
          _this3.paginatorIndexChanged.notify();
        }
      });
      document.getElementById("paginator").addEventListener('click', function (e) {
        var all = document.getElementById("paginator").children;
        [].forEach.call(all, function (item) {
          return item.classList.remove('active');
        });
        var target = e.target;
        target.classList.add('active');
        _this3.currentPage = (parseInt(target.innerHTML) - 1) * 15;
        _this3.pageIndexChanged.notify();
      });
      document.getElementById("newer").addEventListener('click', function (e) {
        if (_this3.currentPaginatorIndex <= _this3.totalPages - 10) {
          _this3.currentPage += 15 * 10;
          _this3.currentPaginatorIndex += 10;
          _this3.pageIndexChanged.notify();
          _this3.paginatorIndexChanged.notify();
        }
      });
      document.getElementById("older").addEventListener('click', function (e) {
        if (_this3.currentPaginatorIndex >= 10) {
          _this3.currentPaginatorIndex -= 10;
          _this3.currentPage -= 15 * 10;
          _this3.pageIndexChanged.notify();
          _this3.paginatorIndexChanged.notify();
        }
      });
    }
  }]);

  return PhotoGallery;
}();

exports.default = PhotoGallery;

/***/ })
/******/ ]);