import Page from './Page.js';
import PhotoGallery from './PhotoGallery';
let photoGallery = new PhotoGallery();
let page = new Page();
page.dataAvailable.attach(()=>{
  photoGallery.model = page.data;
  photoGallery.modelChanged.notify();
  photoGallery.addEventListeners();
});
page.fetchData();
window.onload = ()=>{
  //filter listeners are added here.
document.getElementById("filter-id").addEventListener('change',(e)=>{
    if(e.target.value == "Odd"){
      photoGallery.model = page.data.filter(item=>item.id%2 != 0);
      photoGallery.modelChanged.notify();
    }else if(e.target.value == "Even"){
      photoGallery.model = page.data.filter(item=>item.id%2 == 0);
      photoGallery.modelChanged.notify();
    }
    else{
      photoGallery.model = page.data;
      photoGallery.modelChanged.notify();
    }
});

document.getElementById('search-btn').addEventListener('click',(e)=>{
    let searchString = document.getElementById('filter-title').value;
    photoGallery.model = page.data.filter( item => item.title.includes(searchString));
    photoGallery.modelChanged.notify();

});
}
