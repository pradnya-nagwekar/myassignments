let canvasMap = new Map();
document.addEventListener('mouseup', mouseUpHandler);

function generateCanvas(){
    let totalCanvas = document.getElementsByTagName('canvas').length;
    let n = Math.floor(2.5+3*Math.random()); //random integer between 3 and 5;
    for(i=0;i<n;i++){
        let newCanvas = document.createElement('canvas');   //create new canvas element
        newCanvas.id = `canvas${totalCanvas+1}`;
        var option = document.createElement('option');      //add canvas id option in select list
        option.text = newCanvas.id;
        document.getElementById('canvas-id').add(option);
        document.getElementById('canvas-container').appendChild(newCanvas);

        fnewCanvas = new fabric.Canvas(newCanvas.id);      //create fabric canvas instance
        fnewCanvas.on('mouse:down', mouseDownHandler);     // assign down handler for object select
        canvasMap.set(newCanvas.id,fnewCanvas);            //save fabric canvas instance in Map
        totalCanvas++;
    }
    document.getElementById('generate-shape').removeAttribute('disabled');
}

function mouseDownHandler(evt){
    if(this.getActiveObject()) {
          activeObject = $.extend({}, this.getActiveObject());
          initialCanvas = this.lowerCanvasEl.id;
    }
}

function mouseUpHandler(evt){
  if(evt.target.localName === 'canvas' && initialCanvas) {
      canvasId = evt.target.previousSibling.getAttribute('id');
      if(canvasId !== initialCanvas) {
        activeObject.set({left:evt.layerX,top:evt.layerY});
        canvasMap.get(canvasId).add(activeObject);
        canvasMap.get(canvasId).renderAll();
      }
  }
  initialCanvas = '';
  activeObject  = {};
}

function generateShape(){
  let canvasId = document.getElementById('canvas-id').value;
  if(canvasId != ""){
    let canvasEl = canvasMap.get(canvasId);
    let shape = document.getElementById('shape').value;
    switch (shape) {
      case "Rectangle":
        let rect = new fabric.Rect({
          left: 150,
          top: 20,
          fill: 'red',
          width: 20,
          height: 20,
        })
        canvasEl.add(rect);
        break;
      case "Circle":
        let circle = new fabric.Circle({
          radius: 20,
          fill: 'green',
          top:100,
          left:100,
        });
        canvasEl.add(circle);
        break;
      case "Triangle":
        let triangle = new fabric.Triangle({
          width: 20,
          height: 30,
          fill: 'blue',
          top:50,
          left:50
        });
        canvasEl.add(triangle);
        break;
    }
  }else {
    alert("Please select canvas ID");
  }

}
