//file for http request call
export default class RequestData{
  getData(url){
    return new Promise((resolve,reject)=>{
      let xhttp = new XMLHttpRequest();
      xhttp.open('GET',url);
      xhttp.onload = ()=>{
        if(xhttp.status == 200){
          resolve(xhttp.response);
        }
        else{
          reject(new Error(request.statusText));
        }
      }
      xhttp.onerror = ()=>{
        reject(new Error('Network Error'));
      }
      //Make a RequestData
      xhttp.send();
    });
  }
}
