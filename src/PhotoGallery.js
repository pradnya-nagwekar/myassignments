import Event from './Event';

export default class PhotoGallery{

  constructor(){ //constructor function. Initializes all data
    this.model  = [];
    this.pageIndexChanged = new Event();
    this.paginatorIndexChanged = new Event();
    this.modelChanged = new Event();
    this.currentPage=0;
    this.totalPages = 0;
    this.currentPaginatorIndex = 0;
    this.pageIndexChanged.attach(()=>{ //listener for pageIndexChangedEvent
    this.renderImageData(this.currentPage);
    });
    this.paginatorIndexChanged.attach(()=>{  //listerner for paginator index changed event
      this.renderPaginator(this.currentPaginatorIndex);
    });

    this.modelChanged.attach(()=>{    //listener for model changed event
      this.resetModelData();
    })
  }

  resetModelData(){
    this.currentPage = 0;
    this.currentPaginatorIndex = 0;
    this.totalPages = parseInt(this.model.length/15);
    this.pageIndexChanged.notify();
    this.paginatorIndexChanged.notify();

  }

  //This function does the rendering of images when page index changes
  renderImageData(from){
    let thumbContainers = document.getElementById("photo-gallery-container").children;
   [].forEach.call(thumbContainers,(item,index)=>{
     item.children[0].src = this.model[from+index].url;
     item.children[1].innerHTML = this.model[from+index].title;
     item.children[2].innerHTML = this.model[from+index].id;
   });
  }

  //this function render's paginator data
  renderPaginator(from){
    let paginatorContainer = document.getElementById('paginator').children;
    [].forEach.call(paginatorContainer,(item,index)=>{
      item.innerHTML = from+index+1;
    })
  }

  //add listeners for next, previous, older and newer buttons.
  addEventListeners(){
    document.getElementById('next').addEventListener('click',e=>{
      if(this.currentPage != this.totalPages){
          this.currentPage += 15;
          this.currentPaginatorIndex+=1;
        this.pageIndexChanged.notify();
        this.paginatorIndexChanged.notify();
    }
  });
    document.getElementById('prev').addEventListener('click',e=>{
      if(this.currentPage != 0 && this.currentPaginatorIndex>0){
      this.currentPage -= 15;
      this.currentPaginatorIndex-=1;
      this.pageIndexChanged.notify();
        this.paginatorIndexChanged.notify();
    }
    })
    document.getElementById("paginator").addEventListener('click',(e)=>{
      let all = document.getElementById("paginator").children;
      [].forEach.call(all,item=>item.classList.remove('active'));
      let target = e.target;
        target.classList.add('active');
        this.currentPage = (parseInt(target.innerHTML)-1)*15;
        this.pageIndexChanged.notify();

    });
    document.getElementById("newer").addEventListener('click',e=>{
          if(this.currentPaginatorIndex <= this.totalPages-10){
              this.currentPage += 15*10;
              this.currentPaginatorIndex +=10;
              this.pageIndexChanged.notify();
              this.paginatorIndexChanged.notify();
            }

    })
    document.getElementById("older").addEventListener('click',e=>{
        if(this.currentPaginatorIndex>=10){
          this.currentPaginatorIndex -=10;
          this.currentPage -= 15*10;
          this.pageIndexChanged.notify();
          this.paginatorIndexChanged.notify();
            }
    })
  }
}
