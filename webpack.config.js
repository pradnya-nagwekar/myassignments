var path=require('path');
var webpack=require('webpack');

module.exports={
  entry:path.resolve(__dirname,'./src/app.js'),
  output:{
    path: path.resolve(__dirname,'./build'),
    filename:'app.all.js'
  },
  module:{
    loaders:[{
      test: /\.js$/,
      include: path.resolve(__dirname,"src"),
      loader:'babel-loader',
      query:{
        presets:['es2015']
      }
    }]
  },

  watch:true
}
